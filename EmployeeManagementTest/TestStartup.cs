﻿using EmployeeManagement.Config;
using EmployeeManagement;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace EmployeeManagementTest
{
    /// <summary>
    /// Uses InMemoryDB instead of the local DB, plus other fakes
    /// </summary>
    public class TestStartup : Startup
    {
        public TestStartup(IConfiguration configuration) : base(configuration)
        {
        }

        public void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            // Register the InMemory database context
            services.AddDbContext<EmployeeManagementContext>(options =>
                options.UseInMemoryDatabase("TestDb"));
        }
    }

}
