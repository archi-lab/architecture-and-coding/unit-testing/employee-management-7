Push-Location "../svelte-app"
npm install
npm run build
Pop-Location
Copy-Item -Path "../svelte-app/public/*" -Destination "./wwwroot" -Recurse -Force