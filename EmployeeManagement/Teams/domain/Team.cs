﻿using EmployeeManagement.Employees.domain;

namespace EmployeeManagement.Teams.domain
{
    public class Team
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ICollection<Employee> Employees { get; set; }
    }
}
