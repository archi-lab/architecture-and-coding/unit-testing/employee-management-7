﻿namespace EmployeeManagement.Employees.application
{
    public class EmployeeTreeViewDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string? Role { get; set; }
        public char? PayGrade { get; set; }
        public int HierarchyLevel { get; set; }
       
        public List<EmployeeTreeViewDto> Children { get; set; }
    }
}
