﻿using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Config
{
    public abstract class InMemoryDbContext : DbContext
    {
        public string DbPath { get; }

        public InMemoryDbContext() : base()
        {
            DbPath = "MyLocalDatabase.db";
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseInMemoryDatabase($"Data Source={DbPath}");
    }
}
