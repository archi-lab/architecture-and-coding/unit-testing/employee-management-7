﻿using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Config
{
    public abstract class SQLServerDbContext: DbContext
    {
        public SQLServerDbContext() : base() { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=EmployManagementDb;Trusted_Connection=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }
    }
}
